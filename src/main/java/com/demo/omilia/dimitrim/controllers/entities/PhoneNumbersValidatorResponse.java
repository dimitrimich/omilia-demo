package com.demo.omilia.dimitrim.controllers.entities;

import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author Dimitris Michailidis Response Object returned from ValidatePhoneNumbersController Contains the validation for
 * a list of Phone Numbers
 */
public class PhoneNumbersValidatorResponse {

    @NotNull
    @NotEmpty
    private Set<String> phoneNumbersValidations;

    public Set<String> getPhoneNumbersValidations() {
        return phoneNumbersValidations;
    }

    public void setPhoneNumbersValidations(Set<String> validatedNumbers) {
        this.phoneNumbersValidations = validatedNumbers;
    }
}
