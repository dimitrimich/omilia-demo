package com.demo.omilia.dimitrim.controllers;

import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorRequest;
import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorResponse;
import com.demo.omilia.dimitrim.services.interfaces.validator.IValidatePhoneNumberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.demo.omilia.dimitrim.constants.Constants.VALIDATOR_PATH_MAPPING;

/**
 * @author Dimitris Michailidis
 */
@RestController
public class ValidatePhoneNumbersController {

    @Autowired
    IValidatePhoneNumberService service;

    @PostMapping(value = VALIDATOR_PATH_MAPPING)
    public @ResponseBody
    PhoneNumbersValidatorResponse validatePhoneNumbers(
        @RequestBody @Valid PhoneNumbersValidatorRequest phoneNumbersRequest) {

        return service.validatePhoneNumbers(phoneNumbersRequest);
    }
}
