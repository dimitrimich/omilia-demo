package com.demo.omilia.dimitrim.controllers;

import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorRequest;
import com.demo.omilia.dimitrim.controllers.entities.ValidateIntrepretationsResponse;
import com.demo.omilia.dimitrim.services.interfaces.interpreter.IInterpretPhoneNumberVariations;
import com.demo.omilia.dimitrim.services.interfaces.validator.IValidatePhoneNumberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import static com.demo.omilia.dimitrim.constants.Constants.INTERPRETATIONS_PATH_MAPPING;

/**
 * @author Dimitris Michailidis
 */
@RestController
public class InterpretAndValidatePhoneNumbersController {

    @Autowired
    IValidatePhoneNumberService validationService;

    @Autowired
    IInterpretPhoneNumberVariations interpretationService;

    @PostMapping(value = INTERPRETATIONS_PATH_MAPPING)
    public @ResponseBody
    ValidateIntrepretationsResponse interpretAndValidatePhoneNumbers(
        @RequestBody @Valid PhoneNumbersValidatorRequest phoneNumbersRequest) {

        // find all variations (not validated yet)
        Map<String, Set<String>> phoneNumberVariations = interpretationService.interpretPhoneNumbers(phoneNumbersRequest);

        // validate all variations
        return validationService.validatePhoneNumberInterpretations(phoneNumberVariations);
    }
}
