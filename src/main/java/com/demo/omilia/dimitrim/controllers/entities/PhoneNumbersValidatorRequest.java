package com.demo.omilia.dimitrim.controllers.entities;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author Dimitris Michailidis
 * Object received at ValidatePhoneNumbersController Contains a list of Strings
 * representing phone numbers
 */
public class PhoneNumbersValidatorRequest {
    @NotNull
    @NotEmpty
    private List<String> phoneNumbers;

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }
    public void setPhoneNumbers(List<String> pNumbers){
        this.phoneNumbers = pNumbers;
    }

}
