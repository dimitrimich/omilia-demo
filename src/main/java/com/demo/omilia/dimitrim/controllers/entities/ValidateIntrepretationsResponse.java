package com.demo.omilia.dimitrim.controllers.entities;

import java.util.Map;
import java.util.Set;

/**
 * @author Dimitris Michailidis
 * Response to be returnd from InterpretAndValidatePhoneNumbersController
 * SHould include a Map of numbers that have been validated
 */
public class ValidateIntrepretationsResponse {
    private Map<String, Set<String>> interpretedAndValidatedPhoneNumbers;

    public Map<String, Set<String>> getInterpretedAndValidatedPhoneNumbers() {
        return interpretedAndValidatedPhoneNumbers;
    }

    public void setInterpretedAndValidatedPhoneNumbers(Map<String, Set<String>> interpretedAndValidatedPhoneNumbers) {
        this.interpretedAndValidatedPhoneNumbers = interpretedAndValidatedPhoneNumbers;
    }
}

