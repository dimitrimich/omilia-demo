package com.demo.omilia.dimitrim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NumbersBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(NumbersBackendApplication.class, args);
	}

}
