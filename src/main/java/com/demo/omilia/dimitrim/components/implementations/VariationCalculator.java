package com.demo.omilia.dimitrim.components.implementations;

import com.demo.omilia.dimitrim.components.interfaces.IVariationCalculator;
import com.demo.omilia.dimitrim.constants.Constants;
import com.demo.omilia.dimitrim.utils.NumberUtils;

import org.springframework.stereotype.Component;

/**
 * @author Dimitris Michailidis
 */
@Component
public class VariationCalculator implements IVariationCalculator {

    @Override
    /**
     * Calculate various variations of 3 consecutive groups of digits
     * Scenarios vary on 2 things, existence of zero ( 0 ) and length of each group
     * @return InterpeterVariation enum that signifies which variation was found
     * */
    public Constants.InterpeterVariation calculateVariation(String firstNumber, String secondNumber, String thirdNumber) {
        Constants.InterpeterVariation variationFound = null;

        if (firstNumber.length() == 1) {
            // i.e: 9+8+9 should return only 9
            variationFound = Constants.InterpeterVariation.VARIATION_X_X_X;
        }

        if (firstNumber.length() == 2) {
            variationFound = getInterpeterVariationFor2Digits(firstNumber, secondNumber);

        } else if (firstNumber.length() == 3) {
            if (NumberUtils.isDivisibleBy10(firstNumber) && !NumberUtils.isDivisibleBy100(firstNumber)) {
                variationFound = getInterpeterVariationDivisibleBy10(secondNumber);
            }
            if (!NumberUtils.isDivisibleBy10(firstNumber) && !NumberUtils.isDivisibleBy100(firstNumber)) {
                variationFound = getInterpeterVariationForNotDivisibleBy10or100(firstNumber);
            }
            if (NumberUtils.isDivisibleBy100(firstNumber)) {
                variationFound = getInterpeterVariationForDivisibleBy100(secondNumber, thirdNumber, variationFound);
            }
        }
        return variationFound;
    }

    /**
     * Get interpretations for first number divisible by 10 and not 100
     *  */
    private Constants.InterpeterVariation getInterpeterVariationDivisibleBy10(String secondNumber) {
        Constants.InterpeterVariation variationFound;//i.e: 410 + 4 variations found are 410 and 414
        if (secondNumber.length() == 1) {
            variationFound = Constants.InterpeterVariation.VARIATION_XX0_X_X;
        } else {
            //i.e: 030 + 444 variations found are 030 (second and third number ignored)
            variationFound = Constants.InterpeterVariation.VARIATION_XX0_XXX_X;
        }
        return variationFound;
    }

    /**
     * Get interpretations for first number not divisible by 10 and 100
     */
    private Constants.InterpeterVariation getInterpeterVariationForNotDivisibleBy10or100(String firstNumber) {
        Constants.InterpeterVariation variationFound;
        int intRepresentation = Integer.parseInt(firstNumber);
        if (intRepresentation < 10) {
            //i.e: 003 + 45 + 4 variations found 003 ( second & third number ignored )
            variationFound = Constants.InterpeterVariation.VARIATION_00X_XX_X;

        } else if (intRepresentation < 100) {
            //i.e: 025 + 45 + 4 variations found 025, 0205 ( second & third number ignored )
            variationFound = Constants.InterpeterVariation.VARIATION_0XX_XX_X;

        } else {
            //i.e: 412 + 45 + 4 variations found 412, 40012, 4102, 400102 ( second & third number ignored )
            variationFound = Constants.InterpeterVariation.VARIATION_XXX_XX_X;
        }
        return variationFound;
    }

    /**
     * Get interpretations for first number divisible by 100
     */
    private Constants.InterpeterVariation getInterpeterVariationForDivisibleBy100(String secondNumber, String thirdNumber, Constants.InterpeterVariation variationFound) {
        // i.e. 400+400+6 variations found are just 400
        if (secondNumber.length() == 3) {
            variationFound = Constants.InterpeterVariation.VARIATION_X00_XXX_X;

        } else {
            if (NumberUtils.isDivisibleBy10(secondNumber)) {
                //i.e: 400 + 10 + 5 variations found are 400105, 415, 40015, 4105,
                if (thirdNumber.length() == 1) {
                    variationFound = Constants.InterpeterVariation.VARIATION_X00_X0_X;

                } else {
                    //i.e: 400 + 10 + 45 variations found are 40010, 410 (third number ignored)
                    variationFound = Constants.InterpeterVariation.VARIATION_X00_X0_XX;
                }
            } else {
                if (secondNumber.length() == 2) {
                    //i.e: 400 + 15 + 6 variations found are 400105, 40015, 415, 4105 (third number ignored)
                    variationFound = Constants.InterpeterVariation.VARIATION_X00_XX_X;
                }

                if (secondNumber.length() == 1) {
                    //i.e: 400 + 2 + 6 variations found are 400, 402 (third number ignored)
                    variationFound = Constants.InterpeterVariation.VARIATION_X00_X_X;
                }
            }
        }
        return variationFound;
    }

    /**
     * Get interpretations for first number of length 2
     */
    private Constants.InterpeterVariation getInterpeterVariationFor2Digits(String firstNumber, String secondNumber) {
        Constants.InterpeterVariation variationFound;
        if (NumberUtils.isDivisibleBy10(firstNumber)) {
            if (secondNumber.length() == 1) {
                // i.e: 40+8+9 should return 408 and 48 ( ignore third number)
                variationFound = Constants.InterpeterVariation.VARIATION_X0_X_X;

            } else {
                // i.e: 40+89+9 should return 40 ( ignore second and third number)
                variationFound = Constants.InterpeterVariation.VARIATION_X0_XX_X;
            }
        } else {
            if (Integer.parseInt(firstNumber) < 10) {
                //i.e: 03 + 45 + 4 variations found 03 ( second & third number ignored )
                variationFound = Constants.InterpeterVariation.VARIATION_X_X_X;
            } else {
                // i.e: 45+8+9 should return 405 and 45 ( ignore second and third number)
                variationFound = Constants.InterpeterVariation.VARIATION_XX_X_X;
            }
        }
        return variationFound;
    }
}
