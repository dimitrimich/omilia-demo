package com.demo.omilia.dimitrim.components.implementations;

import java.util.Set;

/**
 * @author Dimitris Michailidis
 */
public class InterpreterPartialVariations {

    /**
     * Set of variations produced from a given group of digits
     */
    private Set<String> variations;
    /**
     * Amount of numbers consumed to create these variations. As for a group of 3 numbers we might consume only the first
     * number or all 3 numbers. Depends on the specific numbers
     */
    private int numberPartsConsumed;

    public Set<String> getVariations() {
        return variations;
    }

    public void setVariations(Set<String> variations) {
        this.variations = variations;
    }

    public int getNumberPartsConsumed() {
        return numberPartsConsumed;
    }

    public void setNumberPartsConsumed(int numberPartsConsumed) {
        this.numberPartsConsumed = numberPartsConsumed;
    }
}
