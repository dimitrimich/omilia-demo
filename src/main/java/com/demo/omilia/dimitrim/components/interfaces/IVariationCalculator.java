package com.demo.omilia.dimitrim.components.interfaces;

import com.demo.omilia.dimitrim.constants.Constants;

/**
 * @author Dimitris Michailidis
 */
public interface IVariationCalculator {
    Constants.InterpeterVariation calculateVariation(String firstNumber, String secondNumber, String thirdNumber);
}
