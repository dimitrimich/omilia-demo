package com.demo.omilia.dimitrim.components.interfaces;

/**
 * @author Dimitris Michailidis
 * Interface which all validators should implement
 * Offers an easy way to add more validation when needed
 */
public interface IPhoneNumberValidator {

    boolean validateString(String phoneNumber);
}
