package com.demo.omilia.dimitrim.components.implementations;

import com.demo.omilia.dimitrim.components.interfaces.IPhoneNumberValidator;
import com.demo.omilia.dimitrim.utils.NumberUtils;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @author Dimitris Michailidis
 *
 * Validates if String is of digits only
 */
@Component
public class PhoneNumberDigitsValidator implements IPhoneNumberValidator {

    @Override
    /** Phone number should not contain whitespace at this point or else exception is thrown */
    public boolean validateString(String phoneNumber) {
        if (StringUtils.containsWhitespace(phoneNumber)) {
            throw new IllegalArgumentException("Phone number (" + phoneNumber + ") should not contain whitespace");
        }

        return NumberUtils.containsOnlyNumbers(phoneNumber);
    }
}
