package com.demo.omilia.dimitrim.components.implementations;

import com.demo.omilia.dimitrim.components.interfaces.IPhoneNumberValidator;
import com.demo.omilia.dimitrim.utils.NumberUtils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author Dimitris Michailidis
 *
 * Validates if String is of expected length
 */
@Component
public class PhoneNumberLengthValidator implements IPhoneNumberValidator {
    @Value("${omilia.phone.number.length.short:10}")
    private int lengthOfPhoneWithoutCountryCode;

    @Value("${omilia.phone.number.length.long:14}")
    private int lengthOfPhoneWithCountryCode;

    @Value("#{'${omilia.phone.number.acceptable.long.prefix.list:00302,003069}'.split(',')}")
    private List<String> longPrefixList;

    @Value("#{'${omilia.phone.number.acceptable.short.prefix.list:2,69}'.split(',')}")
    private List<String> shortPrefixList;

    @Override
    /** Phone number should not contain whitespace at this point or else exception is thrown */
    public boolean validateString(String phoneNumber) {
        if(StringUtils.containsWhitespace(phoneNumber)){
            throw new IllegalArgumentException("Phone number should not contain whitespace");
        }
        boolean prefixMatched = false;

        if (isShortLength(phoneNumber)) {
            prefixMatched = NumberUtils.startsWithPrefixInListBoolean(phoneNumber, shortPrefixList);
        }
        if (isLongLength(phoneNumber)) {
            prefixMatched = NumberUtils.startsWithPrefixInListBoolean(phoneNumber, longPrefixList);
        }

        return prefixMatched;
    }


    private boolean isShortLength(final String phoneNumberWithoutWhiteSpace) {
        return phoneNumberWithoutWhiteSpace.length() == lengthOfPhoneWithoutCountryCode;
    }

    private boolean isLongLength(final String phoneNumberWithoutWhiteSpace) {
        return phoneNumberWithoutWhiteSpace.length() == lengthOfPhoneWithCountryCode;
    }

}
