package com.demo.omilia.dimitrim.components.implementations;

import com.demo.omilia.dimitrim.components.interfaces.IInterpreter;
import com.demo.omilia.dimitrim.components.interfaces.IVariationCalculator;
import com.demo.omilia.dimitrim.constants.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.Set;

import static com.demo.omilia.dimitrim.constants.Constants.ZERO_CHARACTER;

/**
 * @author Dimitris Michailidis
 * Interprets a group of digits into all their variations
 */
@Component
public class Interpreter implements IInterpreter {

    private static final Log LOGGER = LogFactory.getLog(Interpreter.class);

    @Autowired
    IVariationCalculator variationCalculator;

    /**
     * Interprets 3 groups of digits into their variations
     * if secondGroup is empty/null or "0" group is replaced with "000" for simplicity of calculations
     * @param firstNumber first number
     * @param secondNumber second number
     * @param thirdNumber third number
     * @return InterpreterPartialVariations contains a Set of all the variations plus an integer
     * to signify how many numbers where consumed to create these variations
     * */
    public InterpreterPartialVariations handleThreeGroupsOfDigits(String firstNumber, String secondNumber, String thirdNumber) {
        Set<String> possibleInterpretations = new HashSet<>();
        InterpreterPartialVariations variations = new InterpreterPartialVariations();
        variations.setVariations(possibleInterpretations);

        if (StringUtils.isEmpty(secondNumber) || ZERO_CHARACTER.equals(secondNumber)) {
            secondNumber = "000";
        }
        if (StringUtils.isEmpty(thirdNumber) || ZERO_CHARACTER.equals(thirdNumber)) {
            thirdNumber = "000";
        }
        Constants.InterpeterVariation variation = variationCalculator.calculateVariation(firstNumber, secondNumber, thirdNumber);
        if (variation == null) {
            LOGGER.warn("Variation not found correctly for combination of: " + firstNumber + secondNumber + thirdNumber);
            return variations;
        }

        switch (variation) {
            case VARIATION_X_X_X:
                //i.e: 3 + 4 +5 variations found are: 3 (second and third number ignored)
            case VARIATION_X0_XX_X:
            case VARIATION_XX0_XXX_X:
                //i.e: 030 + 444 variations found are: 030 (second and third number ignored)
            case VARIATION_X00_XXX_X:
                // i.e. 400+897+6 variations found are: just 400 (second and third number ignored)
            case VARIATION_00X_XX_X:
                //i.e: 003 + 22 + 6 returns 003 (second and third number ignored)
                possibleInterpretations.add(firstNumber);
                variations.setNumberPartsConsumed(1);
                break;
            case VARIATION_XX_X_X:
                possibleInterpretations.add(firstNumber);
                possibleInterpretations.add(firstNumber.substring(0, 1) + ZERO_CHARACTER + firstNumber.substring(1, 2));
                variations.setNumberPartsConsumed(1);
                break;
            case VARIATION_X0_X_X:
                // i.e: 40+8+9 should return 408 and 48 ( ignore third number)
            case VARIATION_X00_X0_XX:
                //i.e: 400 + 10 + 45 returns 40010, 410 (third number ignored)
                possibleInterpretations.add(firstNumber + secondNumber);
                possibleInterpretations.add(firstNumber.substring(0, 1) + secondNumber);
                variations.setNumberPartsConsumed(2);
                break;
            case VARIATION_XX0_X_X:
                possibleInterpretations.add(firstNumber + secondNumber);
                possibleInterpretations.add(firstNumber.substring(0, 2) + secondNumber);
                variations.setNumberPartsConsumed(2);
                break;
            case VARIATION_X00_X0_X:
                //i.e: 400 + 10 + 5 variations found are: 400105, 415, 40015, 4105,
                possibleInterpretations.add(firstNumber + secondNumber + thirdNumber);
                possibleInterpretations.add(firstNumber.substring(0, 1) + secondNumber.substring(0, 1) + thirdNumber);
                possibleInterpretations.add(firstNumber + secondNumber.substring(0, 1) + thirdNumber);
                possibleInterpretations.add(firstNumber.substring(0, 1) + secondNumber + thirdNumber);
                variations.setNumberPartsConsumed(3);
                break;
            case VARIATION_X00_XX_X:
                //i.e: 400 + 15 + 6 returns 400105, 40015, 415, 4105 (third number ignored)
                possibleInterpretations.add(firstNumber + secondNumber.substring(0, 1) + ZERO_CHARACTER + secondNumber.substring(1));
                possibleInterpretations.add(firstNumber + secondNumber);
                possibleInterpretations.add(firstNumber.substring(0, 1) + secondNumber);
                possibleInterpretations.add(firstNumber.substring(0, 1) + secondNumber.substring(0, 1) + ZERO_CHARACTER + secondNumber.substring(1));
                variations.setNumberPartsConsumed(2);
                break;
            case VARIATION_X00_X_X:
                //i.e: 400 + 2 + 6 returns 400, 402 (third number ignored)
                possibleInterpretations.add(firstNumber);
                possibleInterpretations.add(firstNumber.substring(0, 2) + secondNumber);
                variations.setNumberPartsConsumed(2);
                break;
            case VARIATION_0XX_XX_X:
                //i.e: 025 + 45 + 4 variations found 025, 0205 ( second & third number ignored )
                possibleInterpretations.add(firstNumber);
                possibleInterpretations.add(firstNumber.substring(0, 2) + ZERO_CHARACTER + firstNumber.substring(2, 3));
                variations.setNumberPartsConsumed(1);
                break;
            case VARIATION_XXX_XX_X:
                //i.e: 412 + 45 + 4 variations found 412, 40012, 4102, 400102 ( second & third number ignored )
                possibleInterpretations.add(firstNumber);
                possibleInterpretations.add(firstNumber.substring(0, 1) + ZERO_CHARACTER + ZERO_CHARACTER + firstNumber.substring(1, 3));
                possibleInterpretations.add(firstNumber.substring(0, 1) + firstNumber.substring(1, 2) + ZERO_CHARACTER + firstNumber.substring(2, 3));
                possibleInterpretations.add(firstNumber.substring(0, 1) + ZERO_CHARACTER + ZERO_CHARACTER + firstNumber.substring(1, 2) + ZERO_CHARACTER + firstNumber.substring(2, 3));
                variations.setNumberPartsConsumed(1);
                break;
            default:
                LOGGER.warn("Variation not found correctly for combination of ( " + firstNumber + secondNumber + thirdNumber + ")");
                break;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Groups (" + firstNumber + " " + secondNumber + " " + thirdNumber + ") have given us: " + possibleInterpretations);
        }
        variations.setVariations(possibleInterpretations);
        return variations;
    }

}
