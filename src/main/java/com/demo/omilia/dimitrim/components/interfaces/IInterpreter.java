package com.demo.omilia.dimitrim.components.interfaces;

import com.demo.omilia.dimitrim.components.implementations.InterpreterPartialVariations;

/**
 * @author Dimitris Michailidis
 */
public interface IInterpreter {
    InterpreterPartialVariations handleThreeGroupsOfDigits(String firstNumber, String secondNumber, String thirdNumber);
}
