package com.demo.omilia.dimitrim.components.implementations;

import com.demo.omilia.dimitrim.components.interfaces.IInterpretSinglePhoneNumberVariations;
import com.demo.omilia.dimitrim.components.interfaces.IInterpreter;
import com.demo.omilia.dimitrim.components.interfaces.ITechnicalPhoneNumberValidation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

import static com.demo.omilia.dimitrim.constants.Constants.SPACE_CHARACTER;

/**
 * @author Dimitris Michailidis
 */
@Component
public class InterpretSinglePhoneNumberVariations implements IInterpretSinglePhoneNumberVariations {
    @Autowired
    IInterpreter interpreter;

    @Autowired
    ITechnicalPhoneNumberValidation technicalValidator;

    private static final Log LOGGER = LogFactory.getLog(InterpretSinglePhoneNumberVariations.class);

    /**
     * Avoid infinite loop
     */
    private static final int MAX_ITERATIONS_FAILSAFE = 30;

    @Override
    /**
     * Given a phone number separated by "space" in groups of maximum 3 digits
     * @phoneNumber phone number, separated by single space character, in groups of maximum 3 digits
     * @return the Set of all the possible variations of this given phoneNumber
     * */
    public Set<String> interpretPhoneNumberVariations(String phoneNumber) {

        if (!technicalValidator.isValidToExtractVariations(phoneNumber)) {
            Set<String> returnValue = new HashSet<>();
            returnValue.add(phoneNumber);
            return returnValue;
        }

        Set<String> phoneVariations = new HashSet<>();
        String[] phoneNumberParts = phoneNumber.split(SPACE_CHARACTER);
        // how many parts is this phone number split
        int totalParts = phoneNumberParts.length;

        int index = 0;
        int numberOfIterations = 0;

        while (index < totalParts && numberOfIterations < MAX_ITERATIONS_FAILSAFE) {
            // default values in case index is outside available range
            // helps avoid ArrayIndexOutOfBoundsException
            String firstNumberGroup = "";
            String secondNumberGroup = "";
            String thirdNumberGroup = "";
            if (index == totalParts - 1) {
                firstNumberGroup = phoneNumberParts[index];
            }
            if (index == totalParts - 2) {
                firstNumberGroup = phoneNumberParts[index];
                secondNumberGroup = phoneNumberParts[index + 1];
            }
            if (index <= totalParts - 3) {
                firstNumberGroup = phoneNumberParts[index];
                secondNumberGroup = phoneNumberParts[index + 1];
                thirdNumberGroup = phoneNumberParts[index + 2];
            }
            // Check to avoid numbers longer than 3 digits
            if (firstNumberGroup.length() > 3 || secondNumberGroup.length() > 3 || thirdNumberGroup.length() > 3) {
                LOGGER.warn("Digit group should not be of length greater than 3. (" + firstNumberGroup + " " + secondNumberGroup + " " + thirdNumberGroup + ")");
                return new HashSet<>();
            }

            // get all variations for a group of 3 numbers
            InterpreterPartialVariations partialVariations = interpreter.handleThreeGroupsOfDigits(firstNumberGroup, secondNumberGroup, thirdNumberGroup);
            // append variations to main Set
            phoneVariations = appendNewVariations(phoneVariations, partialVariations);

            index += partialVariations.getNumberPartsConsumed();
            numberOfIterations++;
        }
        return phoneVariations;
    }

    /**
     * Append new variations found to main variations Set
     * @param phoneVariations all the phone variations
     * @param partialVariations variations found for given 3 digit groups
     * @return updated phoneVariations including the new variations found
     * */
    private Set<String> appendNewVariations(Set<String> phoneVariations, InterpreterPartialVariations partialVariations) {
        if (phoneVariations.isEmpty()) {
            // first time add all variations in Set
            phoneVariations.addAll(partialVariations.getVariations());
        } else {
            // concatenate new variations with others found in previous iteration
            Set<String> copySet = new HashSet<>(phoneVariations);
            phoneVariations = new HashSet<>();

            // for all previously found variations
            for (String phoneNumElement : copySet) {
                // for all newly found variations
                for (String variation : partialVariations.getVariations()) {
                    phoneVariations.add(phoneNumElement + variation);
                }
            }
        }
        return phoneVariations;
    }

}
