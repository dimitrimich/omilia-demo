package com.demo.omilia.dimitrim.components.implementations;

import com.demo.omilia.dimitrim.components.interfaces.ITechnicalPhoneNumberValidation;
import com.demo.omilia.dimitrim.utils.NumberUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import static com.demo.omilia.dimitrim.constants.Constants.PHONE_NUMBER_WITH_SPACE;

/**
 * @author Dimitris Michailidis Validate Phone Number to meet technical specifications
 */
@Component
public class TechnicalPhoneNumberValidation implements ITechnicalPhoneNumberValidation {

    private static final Log LOGGER = LogFactory.getLog(TechnicalPhoneNumberValidation.class);

    @Override
    /**
     * Number should not be null/empty
     * Number should not contain multiple spaces
     * */
    public boolean isValidToExtractVariations(String phoneNumber) {
        if (StringUtils.isEmpty(phoneNumber)) {
            LOGGER.info(PHONE_NUMBER_WITH_SPACE + phoneNumber + " ignored as is null or Empty");
            return false;
        }
        String after = phoneNumber.trim().replaceAll(" +", " ");
        if (!phoneNumber.equals(after)) {
            LOGGER.info(PHONE_NUMBER_WITH_SPACE + phoneNumber + " ignored as it contains multiple consecutive spaces instead of 1");
            return false;
        }

        if (!NumberUtils.containsOnlyNumbers(NumberUtils.removeSpaces(phoneNumber))) {
            LOGGER.info(PHONE_NUMBER_WITH_SPACE + phoneNumber + " ignored as it contains letters");
            return false;
        }
        return true;
    }
}
