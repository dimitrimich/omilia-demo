package com.demo.omilia.dimitrim.components.interfaces;

import java.util.Set;

/**
 * @author Dimitris Michailidis
 */
public interface IInterpretSinglePhoneNumberVariations {
    Set<String> interpretPhoneNumberVariations(String phoneNumber);
}
