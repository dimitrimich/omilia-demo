package com.demo.omilia.dimitrim.components.interfaces;

/**
 * @author Dimitris Michailidis
 */
public interface ITechnicalPhoneNumberValidation {

    boolean isValidToExtractVariations(String phoneNumber);

}
