package com.demo.omilia.dimitrim.constants;

/**
 * @author Dimitris Michailidis
 * useful constants
 */
public final class Constants {
    public static final String ONLY_NUMBERS_REGEX = "[0-9]+";

    public static final String VALIDATOR_PATH_MAPPING = "/validate";
    public static final String INTERPRETATIONS_PATH_MAPPING = "/interpretations";
    public static final String INVALID = "INVALID";
    public static final String VALID = "VALID";
    public static final String INVALID_PHONE_NUMBER_PREFIX = " [phone number: " + INVALID + "]";
    public static final String VALID_PHONE_NUMBER_PREFIX = " [phone number: " + VALID + "]";
    public static final String PHONE_NUMBER_WITH_SPACE = "Phone number ";

    public static final String SPACE_CHARACTER = " ";
    public static final String ZERO_CHARACTER = "0";

    /**
     * Various variations of 3 groups of digits.
     * Key thing separating them is the
     * existence of 0 digit in the first two groups
     * Read the enums in the form of:
     *  - "X" means any number
     *  - "0" means a digit of 0
     *  - "_" separates each group
     *  - Amount of "X"s in a group means how many digits are in group
     */
    public enum InterpeterVariation {
        VARIATION_X_X_X,
        VARIATION_XX_X_X,
        VARIATION_X0_X_X,
        VARIATION_X0_XX_X,
        VARIATION_XX0_X_X,
        VARIATION_XX0_XXX_X,
        VARIATION_X00_XXX_X,
        VARIATION_X00_X0_X,
        VARIATION_X00_X0_XX,
        VARIATION_X00_XX_X,
        VARIATION_X00_X_X,
        VARIATION_00X_XX_X,
        VARIATION_0XX_XX_X,
        VARIATION_XXX_XX_X,
    }
}
