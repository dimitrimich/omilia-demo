package com.demo.omilia.dimitrim.utils;

import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.demo.omilia.dimitrim.constants.Constants.ONLY_NUMBERS_REGEX;

/**
 * @author Dimitris Michailidis
 *
 * Various Utilities used throghout the app
 */
public class NumberUtils {

    private NumberUtils() {
        //Utility classes should not have public constructors
    }

    /**
     * Remove spaces from given String
     *
     * @param stringWithSpaces string possibly containing spaces to remove
     * @return string without space characters
     */
    public static String removeSpaces(String stringWithSpaces) {
        if (StringUtils.isEmpty(stringWithSpaces)) {
            throw new IllegalArgumentException("String is empty or null");
        }

        return stringWithSpaces.replace(" ", "");
    }

    /**
     * Check if phone number contains only numbers
     *
     * @param phoneNumberStr Phone number to validate
     * @return boolean value which is true if phoneNumberStr contains only numbers
     */
    public static boolean containsOnlyNumbers(String phoneNumberStr) {
        return phoneNumberStr.matches(ONLY_NUMBERS_REGEX);
    }


    /**
     * Check if string starts with any of the Strings in given List
     *
     * @param stringToCheck   String to check
     * @param allowedPrefixes list of allowed prefixes
     * @return Optional that might contain the prefix if anyone matches
     */
    public static Optional<String> startsWithPrefixInList(String stringToCheck, List<String> allowedPrefixes) {
        return allowedPrefixes
            .stream()
            .filter(stringToCheck::startsWith)
            .findAny();
    }

    /**
     * Check if string starts with any of the Strings in given List
     *
     * @param stringToCheck   String to check
     * @param allowedPrefixes list of allowed prefixes
     * @return True if anyone matches
     */
    public static boolean startsWithPrefixInListBoolean(String stringToCheck, List<String> allowedPrefixes) {
        return startsWithPrefixInList(stringToCheck, allowedPrefixes).isPresent();
    }

    /**
     * checks if number ends with zero, effectively returns true if number % 10 ==0
     *
     * @param number String representation of number
     */
    public static boolean isDivisibleBy10(String number) {
        return Integer.parseInt(number) % 10 == 0;
    }

    /**
     * checks if number ends with zero, effectively returns true if number % 100 ==0
     *
     * @param number String representation of number
     */
    public static boolean isDivisibleBy100(String number) {
        return Integer.parseInt(number) % 100 == 0;
    }

    /**
     * remove spaces from stream
     */
    public static List<String> removeSpaces(Stream<String> numbers) {
        return numbers
            .map(phone -> {
                if (StringUtils.isEmpty(phone)) {
                    return phone;
                }
                return NumberUtils.removeSpaces(phone);
            })
            .collect(Collectors.toList());
    }
}
