package com.demo.omilia.dimitrim.services.interfaces.interpreter;

import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorRequest;

import java.util.Map;
import java.util.Set;

/**
 * @author Dimitris Michailidis
 */
public interface IInterpretPhoneNumberVariations {
    Map<String, Set<String>> interpretPhoneNumbers(PhoneNumbersValidatorRequest request);
}
