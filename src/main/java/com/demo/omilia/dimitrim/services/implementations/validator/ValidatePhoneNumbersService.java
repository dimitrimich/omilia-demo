package com.demo.omilia.dimitrim.services.implementations.validator;

import com.demo.omilia.dimitrim.components.interfaces.IPhoneNumberValidator;
import com.demo.omilia.dimitrim.constants.Constants;
import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorRequest;
import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorResponse;
import com.demo.omilia.dimitrim.controllers.entities.ValidateIntrepretationsResponse;
import com.demo.omilia.dimitrim.services.interfaces.validator.IValidatePhoneNumberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.demo.omilia.dimitrim.utils.NumberUtils.removeSpaces;

/**
 * @author Dimitris Michailidis Validates all numbers in request and generates response
 */
@Service
public class ValidatePhoneNumbersService implements IValidatePhoneNumberService {
    @Autowired
    private List<IPhoneNumberValidator> validatorList;

    @Override
    /** Validate list of phone numbers received in ValidatePhoneNumbersController */
    public PhoneNumbersValidatorResponse validatePhoneNumbers(PhoneNumbersValidatorRequest phoneNumbersRequest) {
        PhoneNumbersValidatorResponse response = new PhoneNumbersValidatorResponse();
        Set<String> validationMessages;

        // remove spaces
        List<String> phoneNumbersTrimmed = removeSpaces(phoneNumbersRequest.getPhoneNumbers().stream());

        // iterate over all phones
        validationMessages = phoneNumbersTrimmed.stream()
            .map(this::validatePhoneNumber)
            .collect(Collectors.toSet());

        response.setPhoneNumbersValidations(validationMessages);

        return response;
    }

    @Override
    public String validatePhoneNumber(String phoneNumber) {
        // iterate over all validators
        String validationMessage = phoneNumber + Constants.VALID_PHONE_NUMBER_PREFIX;
        for (IPhoneNumberValidator iPhoneNumberValidator : validatorList) {
            if (!iPhoneNumberValidator.validateString(phoneNumber)) {
                validationMessage = phoneNumber + Constants.INVALID_PHONE_NUMBER_PREFIX;
            }
        }
        return validationMessage;
    }

    @Override
    /** Validate list of phone numbers received in InterpretAndValidatePhoneNumbersController */
    public ValidateIntrepretationsResponse validatePhoneNumberInterpretations(Map<String, Set<String>> variations) {
        ValidateIntrepretationsResponse response = new ValidateIntrepretationsResponse();
        Map<String, Set<String>> phoneNumbersValidated = new HashMap<>();

        for (Map.Entry<String, Set<String>> singlePhoneAndVariations : variations.entrySet()) {
            String phoneNumber = singlePhoneAndVariations.getKey();

            List<String> trimmedPhoneNumbers = removeSpaces(singlePhoneAndVariations.getValue().stream());

            Set<String> phonesValidated = trimmedPhoneNumbers.stream()
                .map(this::validatePhoneNumber)
                .collect(Collectors.toSet());

            phoneNumbersValidated.put(phoneNumber, phonesValidated);
        }

        response.setInterpretedAndValidatedPhoneNumbers(phoneNumbersValidated);
        return response;
    }
}
