package com.demo.omilia.dimitrim.services.implementations.interpreter;

import com.demo.omilia.dimitrim.components.interfaces.IInterpretSinglePhoneNumberVariations;
import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorRequest;
import com.demo.omilia.dimitrim.services.interfaces.interpreter.IInterpretPhoneNumberVariations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.toMap;

/**
 * @author Dimitris Michailidis
 */
@Service
public class InterpretPhoneNumbersVariations implements IInterpretPhoneNumberVariations {

    @Autowired
    IInterpretSinglePhoneNumberVariations singlePhoneNumberInterpreter;

    @Override
    /** Interpets a list of phone numbers and returns a Set of all possible interpretations
     * @param request PhoneNumbersValidatorRequest object containing all phone numbers
     * @return Map<String, Set<String>> containing all interpretations for each phone number */
    public Map<String, Set<String>> interpretPhoneNumbers(PhoneNumbersValidatorRequest request) {
        return request.getPhoneNumbers()
            .stream()
            .collect(toMap(phoneNumber -> phoneNumber, phoneNumber -> singlePhoneNumberInterpreter.interpretPhoneNumberVariations(phoneNumber)));
    }
}
