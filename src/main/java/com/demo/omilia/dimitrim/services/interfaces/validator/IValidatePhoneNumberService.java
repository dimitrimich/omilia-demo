package com.demo.omilia.dimitrim.services.interfaces.validator;

import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorRequest;
import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorResponse;
import com.demo.omilia.dimitrim.controllers.entities.ValidateIntrepretationsResponse;

import java.util.Map;
import java.util.Set;

/**
 * @author Dimitris Michailidis
 */
public interface IValidatePhoneNumberService {

    PhoneNumbersValidatorResponse validatePhoneNumbers(PhoneNumbersValidatorRequest phoneNumbersRequest);

    String validatePhoneNumber(String phoneNumber);

    ValidateIntrepretationsResponse validatePhoneNumberInterpretations(Map<String, Set<String>> variations);
}
