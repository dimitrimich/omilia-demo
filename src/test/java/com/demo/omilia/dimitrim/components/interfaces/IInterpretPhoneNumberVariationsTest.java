package com.demo.omilia.dimitrim.components.interfaces;

import com.demo.omilia.dimitrim.components.implementations.InterpretSinglePhoneNumberVariations;
import com.demo.omilia.dimitrim.components.implementations.Interpreter;
import com.demo.omilia.dimitrim.components.implementations.TechnicalPhoneNumberValidation;
import com.demo.omilia.dimitrim.components.implementations.VariationCalculator;
import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorRequest;
import com.demo.omilia.dimitrim.services.implementations.interpreter.InterpretPhoneNumbersVariations;
import com.demo.omilia.dimitrim.services.interfaces.interpreter.IInterpretPhoneNumberVariations;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Dimitris Michailidis
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {IInterpretPhoneNumberVariationsTest.IInterpretPhoneNumberVariationsTestContext.class})
public class IInterpretPhoneNumberVariationsTest {
    @Autowired
    IInterpretPhoneNumberVariations underTestService;

    @Test
    public void testInterpretPhoneNumbers_EmptyPhone() {

        PhoneNumbersValidatorRequest request = new PhoneNumbersValidatorRequest();
        List<String> phoneNumbers = new ArrayList<>();
        phoneNumbers.add("");

        request.setPhoneNumbers(phoneNumbers);

        Map<String, Set<String>> actualResponse = underTestService.interpretPhoneNumbers(request);
        final Map<String, Set<String>> expectedResponse = new HashMap<>();
        final String expectedPhoneNumber = "";
        final Set<String> expectedVariations = new HashSet<>();
        expectedVariations.add("");
        expectedResponse.put(expectedPhoneNumber, expectedVariations);

        assertTrue(actualResponse.containsKey(expectedPhoneNumber));
        assertTrue(actualResponse.get(expectedPhoneNumber).size() == 1);
    }

    @Test
    public void testInterpretPhoneNumbers_characters() {

        final String phoneNumber = "gfdge 4gg";
        PhoneNumbersValidatorRequest request = new PhoneNumbersValidatorRequest();
        List<String> phoneNumbers = new ArrayList<>();
        phoneNumbers.add(phoneNumber);

        request.setPhoneNumbers(phoneNumbers);

        Map<String, Set<String>> actualResponse = underTestService.interpretPhoneNumbers(request);

        final Map<String, Set<String>> expectedResponse = new HashMap<>();
        final String expectedPhoneNumber = phoneNumber;
        final Set<String> expectedVariations = new HashSet<>();
        expectedVariations.add(phoneNumber);
        expectedResponse.put(expectedPhoneNumber, expectedVariations);

        assertTrue(actualResponse.containsKey(expectedPhoneNumber));
        assertTrue(actualResponse.get(expectedPhoneNumber).size() == 1);
    }

    @Test
    public void testInterpretPhoneNumbers_MoreSpaces() {

        final String phoneNumber = "34   34   3";
        PhoneNumbersValidatorRequest request = new PhoneNumbersValidatorRequest();
        List<String> phoneNumbers = new ArrayList<>();
        phoneNumbers.add(phoneNumber);

        request.setPhoneNumbers(phoneNumbers);

        Map<String, Set<String>> actualResponse = underTestService.interpretPhoneNumbers(request);

        final Map<String, Set<String>> expectedResponse = new HashMap<>();
        final String expectedPhoneNumber = phoneNumber;
        final Set<String> expectedVariations = new HashSet<>();
        expectedVariations.add(phoneNumber);
        expectedResponse.put(expectedPhoneNumber, expectedVariations);

        assertTrue(actualResponse.containsKey(expectedPhoneNumber));
        assertTrue(actualResponse.get(expectedPhoneNumber).size() == 1);
    }

    @Test
    public void testInterpretPhoneNumbers_fourDigits() {

        final String phoneNumber = "9987 4456";
        PhoneNumbersValidatorRequest request = new PhoneNumbersValidatorRequest();
        List<String> phoneNumbers = new ArrayList<>();
        phoneNumbers.add(phoneNumber);

        request.setPhoneNumbers(phoneNumbers);

        Map<String, Set<String>> actualResponse = underTestService.interpretPhoneNumbers(request);

        final Map<String, Set<String>> expectedResponse = new HashMap<>();
        final String expectedPhoneNumber = phoneNumber;
        final Set<String> expectedVariations = new HashSet<>();
        expectedVariations.add(phoneNumber);
        expectedResponse.put(expectedPhoneNumber, expectedVariations);

        assertTrue(actualResponse.containsKey(expectedPhoneNumber));
        assertTrue(actualResponse.get(expectedPhoneNumber).isEmpty());
    }

    @Test
    public void testInterpretPhoneNumbers_2106930664() {

        final String phoneNumber = "2 10 69 30 6 6 4";
        PhoneNumbersValidatorRequest request = new PhoneNumbersValidatorRequest();
        List<String> phoneNumbers = new ArrayList<>();
        phoneNumbers.add(phoneNumber);

        request.setPhoneNumbers(phoneNumbers);

        Map<String, Set<String>> actualResponse = underTestService.interpretPhoneNumbers(request);
        assertEquals(1, actualResponse.size());

        final Set<String> expectedPhoneNumbers = new HashSet<>();
        expectedPhoneNumbers.add(phoneNumber);
        assertEquals(expectedPhoneNumbers, actualResponse.keySet());

        final Set<String> expectedVariations = new HashSet<>();
        expectedVariations.add("2106930664");
        expectedVariations.add("2106093664");
        expectedVariations.add("210693664");
        expectedVariations.add("21060930664");


        assertEquals(expectedVariations, actualResponse.get(phoneNumber));
    }

    @Test
    public void testInterpretPhoneNumbers_Combination() {

        final String phoneNumber1 = "";
        final String phoneNumber2 = "gfdge 4gg";
        final String phoneNumber3 = "9987 4456";
        final String phoneNumber4 = "34   34   3";
        final String phoneNumber5 = "2 10 69 30 6 6 4";
        PhoneNumbersValidatorRequest request = new PhoneNumbersValidatorRequest();
        List<String> phoneNumbers = new ArrayList<>();
        phoneNumbers.add(phoneNumber1);
        phoneNumbers.add(phoneNumber2);
        phoneNumbers.add(phoneNumber3);
        phoneNumbers.add(phoneNumber4);
        phoneNumbers.add(phoneNumber5);

        request.setPhoneNumbers(phoneNumbers);

        Map<String, Set<String>> actualResponse = underTestService.interpretPhoneNumbers(request);
        assertEquals(5, actualResponse.size());

        final Map<String, Set<String>> expectedResponse = new HashMap<>();
        final Set<String> expectedPhoneNumbers = new HashSet<>();
        expectedPhoneNumbers.add(phoneNumber1);
        expectedPhoneNumbers.add(phoneNumber2);
        expectedPhoneNumbers.add(phoneNumber3);
        expectedPhoneNumbers.add(phoneNumber4);
        expectedPhoneNumbers.add(phoneNumber5);
        assertEquals(expectedPhoneNumbers, actualResponse.keySet());

        final Set<String> expectedVariations = new HashSet<>();
        expectedVariations.add("2106930664");
        expectedVariations.add("2106093664");
        expectedVariations.add("210693664");
        expectedVariations.add("21060930664");

        assertEquals(expectedVariations, actualResponse.get(phoneNumber5));
    }

    static class IInterpretPhoneNumberVariationsTestContext {

        @Bean
        public IInterpretPhoneNumberVariations getIInterpretPhoneNumberVariations() {
            return new InterpretPhoneNumbersVariations();
        }

        @Bean
        public IInterpretSinglePhoneNumberVariations getIInterpretSinglePhoneNumberVariations() {
            return new InterpretSinglePhoneNumberVariations();
        }

        @Bean
        public IVariationCalculator getVariationCalculator() {
            return new VariationCalculator();
        }

        @Bean
        public IInterpreter getInterpreter() {
            return new Interpreter();
        }

        @Bean
        public ITechnicalPhoneNumberValidation getITechnicalPhoneNumberValidation() {
            return new TechnicalPhoneNumberValidation();
        }
    }
}
