package com.demo.omilia.dimitrim.components.implementations;


import com.demo.omilia.dimitrim.utils.NumberUtils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Dimitris Michailidis
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {PhoneNumberLengthValidatorTest.PhoneNumberLengthValidatorTestContext.class})
public class PhoneNumberLengthValidatorTest {

    @Autowired
    private PhoneNumberLengthValidator lengthValidator;


    @Test
    public void testValidateString_NegativeScenario_Length11() {
        final String givenString = "21069306604";
        final boolean isValidLength = lengthValidator.validateString(givenString);
        final String errorMessage = "String " + givenString + " appears valid but is not";

        assertFalse(isValidLength, errorMessage);
    }

    @Test
    public void testValidateString_NegativeScenario_Length3() {
        final String givenString = "214";
        final boolean isValidLength = lengthValidator.validateString(givenString);
        final String errorMessage = "String " + givenString + " appears valid but is not";

        assertFalse(isValidLength, errorMessage);
    }


    @Test
    public void testValidateString_PositiveScenario_Length10() {
        final String givenString = "2106930664";
        final boolean isValidLength = lengthValidator.validateString(givenString);
        final String errorMessage = "String " + givenString + " appears invalid but is not";

        assertTrue(isValidLength, errorMessage);
    }

    @Test
    public void testValidateString_PositiveScenario_Length14() {
        final String givenString = "00306974092252";
        final boolean isValidLength = lengthValidator.validateString(givenString);
        final String errorMessage = "String " + givenString + " appears invalid but is not";

        assertTrue(isValidLength, errorMessage);
    }

    @Test
    public void testValidateString_PDFExample() {
        final String givenString = "2 10 69 30 6 6 4";
        final boolean isValidLength = lengthValidator.validateString(NumberUtils.removeSpaces(givenString));
        final String errorMessage = "String " + givenString + " appears invalid but is not";

        assertTrue(isValidLength, errorMessage);
    }


    static class PhoneNumberLengthValidatorTestContext {
        @Bean
        public PhoneNumberLengthValidator getLengthValidator() {
            return new PhoneNumberLengthValidator();
        }
    }
}
