package com.demo.omilia.dimitrim.components.interfaces;

import com.demo.omilia.dimitrim.components.implementations.VariationCalculator;
import com.demo.omilia.dimitrim.constants.Constants;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Dimitris Michailidis
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {VariationCalculatorTest.VariationCalculatorTestContext.class})
public class VariationCalculatorTest {
    @Autowired
    IVariationCalculator variationCalculator;

    @Test
    public void testCalculateVariation_VARIATION_VARIATION_X00_X_X() {

        final String firstNumber = "400";
        final String secondNumber = "4";
        final String thirdNumber = "3";

        final Constants.InterpeterVariation expectedVariation = Constants.InterpeterVariation.VARIATION_X00_X_X;

        Constants.InterpeterVariation expectedAnswer = variationCalculator.calculateVariation(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedVariation, expectedAnswer);
    }

    @Test
    public void testCalculateVariation_VARIATION_X00_XX_X() {

        final String firstNumber = "400";
        final String secondNumber = "43";
        final String thirdNumber = "3";

        final Constants.InterpeterVariation expectedVariation = Constants.InterpeterVariation.VARIATION_X00_XX_X;

        Constants.InterpeterVariation expectedAnswer = variationCalculator.calculateVariation(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedVariation, expectedAnswer);
    }

    @Test
    public void testCalculateVariation_VARIATION_X00_X0_XX() {

        final String firstNumber = "400";
        final String secondNumber = "40";
        final String thirdNumber = "53";

        final Constants.InterpeterVariation expectedVariation = Constants.InterpeterVariation.VARIATION_X00_X0_XX;

        Constants.InterpeterVariation expectedAnswer = variationCalculator.calculateVariation(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedVariation, expectedAnswer);
    }

    @Test
    public void testCalculateVariation_VARIATION_X00_X0_X() {

        final String firstNumber = "400";
        final String secondNumber = "40";
        final String thirdNumber = "5";

        final Constants.InterpeterVariation expectedVariation = Constants.InterpeterVariation.VARIATION_X00_X0_X;

        Constants.InterpeterVariation expectedAnswer = variationCalculator.calculateVariation(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedVariation, expectedAnswer);
    }

    @Test
    public void testCalculateVariation_VARIATION_X00_XXX_X() {

        final String firstNumber = "400";
        final String secondNumber = "434";
        final String thirdNumber = "5";

        final Constants.InterpeterVariation expectedVariation = Constants.InterpeterVariation.VARIATION_X00_XXX_X;

        Constants.InterpeterVariation expectedAnswer = variationCalculator.calculateVariation(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedVariation, expectedAnswer);
    }

    @Test
    public void testCalculateVariation_VARIATION_XX0_X_X() {

        final String firstNumber = "430";
        final String secondNumber = "4";
        final String thirdNumber = "5";

        final Constants.InterpeterVariation expectedVariation = Constants.InterpeterVariation.VARIATION_XX0_X_X;

        Constants.InterpeterVariation expectedAnswer = variationCalculator.calculateVariation(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedVariation, expectedAnswer);
    }


    @Test
    public void testCalculateVariation_VARIATION_X_X_X() {

        final String firstNumber = "4";
        final String secondNumber = "4";
        final String thirdNumber = "5";

        final Constants.InterpeterVariation expectedVariation = Constants.InterpeterVariation.VARIATION_X_X_X;

        Constants.InterpeterVariation expectedAnswer = variationCalculator.calculateVariation(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedVariation, expectedAnswer);
    }


    @Test
    public void testCalculateVariation_VARIATION_XX_X_X() {

        final String firstNumber = "43";
        final String secondNumber = "4";
        final String thirdNumber = "5";

        final Constants.InterpeterVariation expectedVariation = Constants.InterpeterVariation.VARIATION_XX_X_X;

        Constants.InterpeterVariation expectedAnswer = variationCalculator.calculateVariation(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedVariation, expectedAnswer);
    }

    @Test
    public void testCalculateVariation_VARIATION_X0_X_X() {

        final String firstNumber = "40";
        final String secondNumber = "4";
        final String thirdNumber = "5";

        final Constants.InterpeterVariation expectedVariation = Constants.InterpeterVariation.VARIATION_X0_X_X;

        Constants.InterpeterVariation expectedAnswer = variationCalculator.calculateVariation(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedVariation, expectedAnswer);
    }

    @Test
    public void testCalculateVariation_VARIATION_X0_XX_X() {

        final String firstNumber = "40";
        final String secondNumber = "42";
        final String thirdNumber = "5";

        final Constants.InterpeterVariation expectedVariation = Constants.InterpeterVariation.VARIATION_X0_XX_X;

        Constants.InterpeterVariation expectedAnswer = variationCalculator.calculateVariation(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedVariation, expectedAnswer);
    }


    static class VariationCalculatorTestContext {
        @Bean
        public IVariationCalculator getVariationCalculator() {
            return new VariationCalculator();
        }
    }
}
