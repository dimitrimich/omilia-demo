package com.demo.omilia.dimitrim.components.interfaces;

import com.demo.omilia.dimitrim.components.implementations.InterpretSinglePhoneNumberVariations;
import com.demo.omilia.dimitrim.components.implementations.Interpreter;
import com.demo.omilia.dimitrim.components.implementations.TechnicalPhoneNumberValidation;
import com.demo.omilia.dimitrim.components.implementations.VariationCalculator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Dimitris Michailidis
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {InterpretSinglePhoneNumberVariationsTest.InterpretSinglePhoneNumberVariationsTestContext.class})
public class InterpretSinglePhoneNumberVariationsTest {

    @Autowired
    private InterpretSinglePhoneNumberVariations singlePhoneInterpreter;

    @Test
    public void testInterpretPhoneNumberVariations_emptyPhone() {
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add("");
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations("");

        assertEquals(expectedAnswer, actualAnswer);
    }

    @Test
    public void testInterpretPhoneNumberVariations_nullPhone() {
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add(null);
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(null);

        assertEquals(expectedAnswer, actualAnswer);
    }

    @Test
    public void testInterpretPhoneNumberVariations_3() {
        final String phoneNumber = "3";
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add(phoneNumber);
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(phoneNumber);

        assertEquals(expectedAnswer, actualAnswer);
    }

    @Test
    public void testInterpretPhoneNumberVariations_0030_28_20() {
        final String phoneNumber = "0030 28 20";
        final Set<String> expectedAnswer = new HashSet<>();
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(phoneNumber);

        assertEquals(expectedAnswer, actualAnswer);
    }

    @Test
    public void testInterpretPhoneNumberVariations_003_028_20() {
        final String phoneNumber = "003 028 20";
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add("003020820");
        expectedAnswer.add("00302820");
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(phoneNumber);

        assertEquals(expectedAnswer, actualAnswer);
    }

    @Test
    public void testInterpretPhoneNumberVariations_0_0_3_0_2_8_2_0() {
        final String phoneNumber = "0 0 3 0 2 8 2 0";
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add("00302820");
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(phoneNumber);

        assertEquals(expectedAnswer, actualAnswer);
    }

    @Test
    public void testInterpretPhoneNumberVariations_003_002_82_0() {
        final String phoneNumber = "003 002 82 0";
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add("0030028020");
        expectedAnswer.add("003002820");
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(phoneNumber);

        assertEquals(expectedAnswer, actualAnswer);
    }

    @Test
    public void testInterpretPhoneNumberVariations_03_02_82_0() {
        final String phoneNumber = "03 02 82 0";
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add("03028020");
        expectedAnswer.add("0302820");
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(phoneNumber);

        assertEquals(expectedAnswer, actualAnswer);
    }

    @Test
    public void testInterpretPhoneNumberVariations_030_020_820_0() {
        final String phoneNumber = "030 020 820 0";
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add("0300208200");
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(phoneNumber);

        assertEquals(expectedAnswer, actualAnswer);
    }

    @Test
    public void testInterpretPhoneNumberVariations_030_020_820_5() {
        final String phoneNumber = "030 020 820 5";
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add("0300208205");
        expectedAnswer.add("030020825");
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(phoneNumber);

        assertEquals(expectedAnswer, actualAnswer);
    }

    @Test
    public void testInterpretPhoneNumberVariations_PDF_Example_210693664() {
        final String phoneNumber = "2 10 6 9 30 6 6 4";
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add("2106930664");
        expectedAnswer.add("216930664");
        expectedAnswer.add("210693664");
        expectedAnswer.add("21693664");
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(phoneNumber);

        assertEquals(expectedAnswer, actualAnswer);
    }
    @Test
    public void testInterpretPhoneNumberVariations_PDF_Example_2106930664() {
        final String phoneNumber = "2 10 69 30 6 6 4";
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add("2106930664");
        expectedAnswer.add("210693664");
        expectedAnswer.add("2106093664");
        expectedAnswer.add("21060930664");
        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(phoneNumber);

        assertEquals(expectedAnswer, actualAnswer);
    }

    @Test
    public void testInterpretPhoneNumberVariations_PDF_Example_0030697002413502() {
        final String phoneNumber = "0 0 30 69 700 24 1 3 50 2";
        final Set<String> expectedAnswer = new HashSet<>();
        expectedAnswer.add("003069700241352");
        expectedAnswer.add("003060970020413502");
        expectedAnswer.add("00306972041352");
        expectedAnswer.add("00306097002041352");
        expectedAnswer.add("0030609720413502");
        expectedAnswer.add("0030697002413502");
        expectedAnswer.add("00306097241352");
        expectedAnswer.add("003069720413502");
        expectedAnswer.add("00306970020413502");
        expectedAnswer.add("00306097002413502");
        expectedAnswer.add("0030697002041352");
        expectedAnswer.add("003060972041352");
        expectedAnswer.add("0030609700241352");
        expectedAnswer.add("00306972413502");
        expectedAnswer.add("0030697241352");
        expectedAnswer.add("003060972413502");

        Set<String> actualAnswer = singlePhoneInterpreter.interpretPhoneNumberVariations(phoneNumber);

        assertEquals(expectedAnswer, actualAnswer);
    }


    static class InterpretSinglePhoneNumberVariationsTestContext {
        @Bean
        public IVariationCalculator getVariationCalculator() {
            return new VariationCalculator();
        }

        @Bean
        public IInterpreter getInterpreter() {
            return new Interpreter();
        }

        @Bean
        public InterpretSinglePhoneNumberVariations getInterpretSinglePhoneNumberVariationsTest() {
            return new InterpretSinglePhoneNumberVariations();
        }
        @Bean
        public ITechnicalPhoneNumberValidation getITechnicalPhoneNumberValidation(){
            return new TechnicalPhoneNumberValidation();
        }

    }
}
