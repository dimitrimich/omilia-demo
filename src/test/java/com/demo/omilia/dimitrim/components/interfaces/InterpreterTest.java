package com.demo.omilia.dimitrim.components.interfaces;

import com.demo.omilia.dimitrim.components.implementations.Interpreter;
import com.demo.omilia.dimitrim.components.implementations.InterpreterPartialVariations;
import com.demo.omilia.dimitrim.components.implementations.VariationCalculator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Dimitris Michailidis
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {InterpreterTest.InterpreterTestContext.class})
public class InterpreterTest {
    @Autowired
    IInterpreter interpreter;

    @Test
    public void testHandleThreeDigitNumbers_case_400_10_5() {

        final String firstNumber = "400";
        final String secondNumber = "10";
        final String thirdNumber = "5";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("415");
        expectedAnswers.add("400105");
        expectedAnswers.add("40015");
        expectedAnswers.add("4105");

        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(3, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_400_23_6() {

        final String firstNumber = "400";
        final String secondNumber = "23";
        final String thirdNumber = "6";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("40023");
        expectedAnswers.add("423");
        expectedAnswers.add("400203");
        expectedAnswers.add("4203");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(2, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_400_400() {

        final String firstNumber = "400";
        final String secondNumber = "400";
        final String thirdNumber = "6";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("400");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(1, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_400_23_212() {

        final String firstNumber = "400";
        final String secondNumber = "23";
        final String thirdNumber = "212";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("40023");
        expectedAnswers.add("423");
        expectedAnswers.add("400203");
        expectedAnswers.add("4203");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(2, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_400____() {

        final String firstNumber = "400";
        final String secondNumber = "";
        final String thirdNumber = "";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("400");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(1, variationsFound.getNumberPartsConsumed());
    }


    @Test
    public void testHandleThreeDigitNumbers_case_400_2_2() {

        final String firstNumber = "400";
        final String secondNumber = "2";
        final String thirdNumber = "2";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("400");
        expectedAnswers.add("402");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(2, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_1_2_3() {

        final String firstNumber = "1";
        final String secondNumber = "2";
        final String thirdNumber = "3";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("1");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(1, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_45_2_3() {

        final String firstNumber = "45";
        final String secondNumber = "2";
        final String thirdNumber = "3";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("45");
        expectedAnswers.add("405");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(1, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_10_2_3() {

        final String firstNumber = "10";
        final String secondNumber = "2";
        final String thirdNumber = "3";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("12");
        expectedAnswers.add("102");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(2, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_10_23_3() {

        final String firstNumber = "10";
        final String secondNumber = "23";
        final String thirdNumber = "3";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("10");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(1, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_10_23__() {

        final String firstNumber = "10";
        final String secondNumber = "23";
        final String thirdNumber = "";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("10");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(1, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_003_02_25() {

        final String firstNumber = "003";
        final String secondNumber = "02";
        final String thirdNumber = "25";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("003");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(1, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_03_02_25() {

        final String firstNumber = "03";
        final String secondNumber = "02";
        final String thirdNumber = "25";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("03");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(1, variationsFound.getNumberPartsConsumed());
    }


    @Test
    public void testHandleThreeDigitNumbers_case_410_2_25() {

        final String firstNumber = "410";
        final String secondNumber = "2";
        final String thirdNumber = "25";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("412");
        expectedAnswers.add("4102");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(2, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_412_2_25() {

        final String firstNumber = "412";
        final String secondNumber = "2";
        final String thirdNumber = "25";

        final Set<String> expectedAnswers = new HashSet<>();
        //i.e: 412 + 45 + 4 variations found 412, 40012, 4102, 400102 ( second & third number ignored )
        expectedAnswers.add("412");
        expectedAnswers.add("40012");
        expectedAnswers.add("4102");
        expectedAnswers.add("400102");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(1, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_028_2_25() {

        final String firstNumber = "028";
        final String secondNumber = "2";
        final String thirdNumber = "25";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("028");
        expectedAnswers.add("0208");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(1, variationsFound.getNumberPartsConsumed());
    }

    @Test
    public void testHandleThreeDigitNumbers_case_030_020_25() {

        final String firstNumber = "030";
        final String secondNumber = "020";
        final String thirdNumber = "25";

        final Set<String> expectedAnswers = new HashSet<>();
        expectedAnswers.add("030");
        InterpreterPartialVariations variationsFound = interpreter.handleThreeGroupsOfDigits(firstNumber, secondNumber, thirdNumber);

        assertEquals(expectedAnswers, variationsFound.getVariations());
        assertEquals(1, variationsFound.getNumberPartsConsumed());
    }


    final String phoneNumber = "34   34   3";

    static class InterpreterTestContext {
        @Bean
        public IVariationCalculator getVariationCalculator() {
            return new VariationCalculator();
        }

        @Bean
        public IInterpreter getInterpreter() {
            return new Interpreter();
        }
    }
}
