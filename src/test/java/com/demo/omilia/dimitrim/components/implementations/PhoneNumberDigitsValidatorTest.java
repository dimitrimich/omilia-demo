package com.demo.omilia.dimitrim.components.implementations;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Dimitris Michailidis
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {PhoneNumberDigitsValidatorTest.PhoneNumberDigitsValidatorTestContext.class})
public class PhoneNumberDigitsValidatorTest {

    @Autowired
    private PhoneNumberDigitsValidator digitsValidator;


    @Test
    public void testValidateString_NegativeScenario_whitespace() {
        final String givenString = "21069 306a604";

        assertThrows(IllegalArgumentException.class, () -> digitsValidator.validateString(givenString));
    }

    @Test
    public void testValidateString_PositiveScenario_length10() {
        final String givenString = "2106930660";
        final String errorMessage = "String " + givenString + " is valid, but appears not";
        assertTrue(digitsValidator.validateString(givenString), errorMessage);
    }

    @Test
    public void testValidateString_PositiveScenario_length14() {
        final String givenString = "00306974092252";
        final String errorMessage = "String " + givenString + " is valid, but appears not";
        assertTrue(digitsValidator.validateString(givenString), errorMessage);
    }

    static class PhoneNumberDigitsValidatorTestContext {
        @Bean
        public PhoneNumberDigitsValidator getDigitsValidator() {
            return new PhoneNumberDigitsValidator();
        }
    }
}
