package com.demo.omilia.dimitrim.components.interfaces;

import com.demo.omilia.dimitrim.components.implementations.InterpretSinglePhoneNumberVariations;
import com.demo.omilia.dimitrim.components.implementations.Interpreter;
import com.demo.omilia.dimitrim.components.implementations.TechnicalPhoneNumberValidation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Dimitris Michailidis
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ITechnicalPhoneNumberValidationTest.ITechnicalPhoneNumberValidationTestContext.class})
public class ITechnicalPhoneNumberValidationTest {

    @Autowired
    ITechnicalPhoneNumberValidation numberValidation;

    @Test
    public void testIsValidToExtractVariations() {
        final String phoneNumber = "foo bar";
        final boolean expectedValue = false;
        boolean actualValue = numberValidation.isValidToExtractVariations(phoneNumber);

        assertEquals(expectedValue, actualValue);
    }


    static class ITechnicalPhoneNumberValidationTestContext {
        @Bean
        public ITechnicalPhoneNumberValidation getITechnicalPhoneNumberValidation() {
            return new TechnicalPhoneNumberValidation();
        }
    }
}
