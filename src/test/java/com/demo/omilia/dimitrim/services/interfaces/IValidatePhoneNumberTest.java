package com.demo.omilia.dimitrim.services.interfaces;

import com.demo.omilia.dimitrim.components.implementations.PhoneNumberDigitsValidator;
import com.demo.omilia.dimitrim.components.implementations.PhoneNumberLengthValidator;
import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorRequest;
import com.demo.omilia.dimitrim.controllers.entities.PhoneNumbersValidatorResponse;
import com.demo.omilia.dimitrim.services.implementations.validator.ValidatePhoneNumbersService;
import com.demo.omilia.dimitrim.services.interfaces.validator.IValidatePhoneNumberService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Dimitris Michailidis
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {IValidatePhoneNumberTest.IValidatePhoneNumberTestContext.class})
public class IValidatePhoneNumberTest {

    @Autowired
    IValidatePhoneNumberService validatePhoneNumberService;

    @Test
    public void testValidatePhoneNumbers() {
        // sample request
        final PhoneNumbersValidatorRequest request = new PhoneNumbersValidatorRequest();
        final List<String> phoneNumbersInRequest = new ArrayList<>();
        phoneNumbersInRequest.add("30 2 5 58");
        phoneNumbersInRequest.add("2 10 69 30 6 6 4");
        phoneNumbersInRequest.add("2 10 69 30 6 60 4");
        phoneNumbersInRequest.add("0 0 30 69 74 0 9 22 52");
        phoneNumbersInRequest.add("0 0 30 69 74 0 9 22 52a");
        request.setPhoneNumbers(phoneNumbersInRequest);

        // expected response
        final Set<String> expectedValidationResponses = new HashSet<>();
        expectedValidationResponses.add("302558 [phone number: INVALID]");
        expectedValidationResponses.add("2106930664 [phone number: VALID]");
        expectedValidationResponses.add("21069306604 [phone number: INVALID]");
        expectedValidationResponses.add("00306974092252 [phone number: VALID]");
        expectedValidationResponses.add("00306974092252a [phone number: INVALID]");

        // call
        PhoneNumbersValidatorResponse response = validatePhoneNumberService.validatePhoneNumbers(request);

        // assert
        assertEquals(expectedValidationResponses, response.getPhoneNumbersValidations());
    }

    static class IValidatePhoneNumberTestContext {
        @Bean
        public IValidatePhoneNumberService getValidatePhoneNumber() {
            return new ValidatePhoneNumbersService();
        }

        @Bean
        public PhoneNumberLengthValidator getLengthValidator() {
            return new PhoneNumberLengthValidator();
        }

        @Bean
        public PhoneNumberDigitsValidator getDigitsValidator() {
            return new PhoneNumberDigitsValidator();
        }

    }
}
