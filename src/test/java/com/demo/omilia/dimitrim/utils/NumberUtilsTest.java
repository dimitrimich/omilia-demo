package com.demo.omilia.dimitrim.utils;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Dimitris Michailidis
 */
public class NumberUtilsTest {

    @Test
    public void testSpaceRemovalSingleSpace() {
        final String givenString = "4234234 356546";
        final String expectedString = "4234234356546";

        assertEquals(expectedString, NumberUtils.removeSpaces(givenString), "Single space removal was not a success. " + expectedString + " didn't match " + NumberUtils.removeSpaces(givenString));
    }

    @Test
    public void testSpaceRemovalDoubleSpace() {
        final String givenString = "4234234  356546";
        final String expectedString = "4234234356546";

        assertEquals(expectedString, NumberUtils.removeSpaces(givenString), "Double space removal was not a success. " + expectedString + " didn't match " + NumberUtils.removeSpaces(givenString));
    }

    @Test
    public void testSpaceRemovalMultipleSpace() {
        final String givenString = "  42 34234 356  546 ";
        final String expectedString = "4234234356546";

        assertEquals(expectedString, NumberUtils.removeSpaces(givenString), "Multiple space removal was not a success. " + expectedString + " didn't match " + NumberUtils.removeSpaces(givenString));
    }

    @Test
    public void testSpaceRemovalNoSpace() {
        final String givenString = "4234234356546";
        final String expectedString = "4234234356546";

        assertEquals(expectedString, NumberUtils.removeSpaces(givenString), "No space removal was not a success. " + expectedString + " didn't match " + NumberUtils.removeSpaces(givenString));
    }
    @Test
    public void testSpaceRemovalPDFExample() {
        final String givenString = "2 10 69 30 6 6 4";
        final String expectedString = "2106930664";

        assertEquals(expectedString, NumberUtils.removeSpaces(givenString), "No space removal was not a success. " + expectedString + " didn't match " + NumberUtils.removeSpaces(givenString));
    }


    @Test
    public void testSpaceRemovalValidation() {
        final String givenString = "";
        assertThrows(IllegalArgumentException.class, () -> NumberUtils.removeSpaces(givenString));
    }

    @Test
    public void testContainsOnlyNumbers_BasicScenario() {
        final String givenString = "4234a234356546";

        assertFalse(NumberUtils.containsOnlyNumbers(givenString), "String contains characters but we didn't find that" + givenString);
    }

    @Test
    public void testContainsOnlyNumbers_BasicScenario1() {
        final String givenString = " 4234234356546";

        assertFalse(NumberUtils.containsOnlyNumbers(givenString), "String contains characters but we didn't find that" + givenString);
    }

    @Test
    public void testContainsOnlyNumbers_WhitespaceScenario() {
        final String givenString = " 423423435 6546";

        assertFalse(NumberUtils.containsOnlyNumbers(givenString), "String contains characters but we didn't find that" + givenString);
    }

    @Test
    public void testContainsOnlyNumbers_BasicScenario2() {
        final String givenString = "a4234a234356546";

        assertFalse(NumberUtils.containsOnlyNumbers(givenString), "String contains characters but we didn't find that" + givenString);
    }

    @Test
    public void testContainsOnlyNumbers_PositiveScenario() {
        final String givenString = "42342344565756";

        assertTrue(NumberUtils.containsOnlyNumbers(givenString), "String doesn't contain characters but we didn't find that" + givenString);
    }

    @Test
    public void testContainsOnlyNumbers_PDFExample() {
        final String givenString = "2 10 69 30 6 6 4";

        assertTrue(NumberUtils.containsOnlyNumbers(NumberUtils.removeSpaces(givenString)), "String doesn't contain characters but we didn't find that" + givenString);
    }

    @Test
    public void testStartsWithPrefixInList_PositiveScenario() {
        final String givenString = "42342344565756";
        final List<String> allowedPrefixes = Lists.newArrayList("4", "324");
        final String errorMessage = "String "+givenString+" starts with allowed prefix but not detected";

        assertTrue(NumberUtils.startsWithPrefixInList(givenString, allowedPrefixes).isPresent(), errorMessage);
    }

    @Test
    public void testStartsWithPrefixInList_PDFExample() {
        final String givenString = "2 10 69 30 6 6 4";
        final List<String> allowedPrefixes = Lists.newArrayList("2", "69");
        final String errorMessage = "String "+givenString+" starts with allowed prefix but not detected";

        assertTrue(NumberUtils.startsWithPrefixInList(NumberUtils.removeSpaces(givenString), allowedPrefixes).isPresent(), errorMessage);
    }

    @Test
    public void testStartsWithPrefixInList_PositiveScenario1() {
        final String givenString = "00306942342344565756";
        final List<String> allowedPrefixes = Lists.newArrayList("00302", "003069");
        final String errorMessage = "String "+givenString+" starts with allowed prefix but not detected";

        assertTrue(NumberUtils.startsWithPrefixInList(givenString, allowedPrefixes).isPresent(), errorMessage);
    }


    @Test
    public void testStartsWithPrefixInList_PositiveScenario2() {
        final String givenString = "6942342344565756";
        final List<String> allowedPrefixes = Lists.newArrayList("2", "69");
        final String errorMessage = "String "+givenString+" starts with allowed prefix but not detected";

        assertTrue(NumberUtils.startsWithPrefixInList(givenString, allowedPrefixes).isPresent(), errorMessage);
    }

    @Test
    public void testStartsWithPrefixInList_NegativeScenario1() {
        final String givenString = "6942342344565756";
        final List<String> allowedPrefixes = Lists.newArrayList("456", "5666");
        final String errorMessage = "String "+givenString+" starts with allowed prefix but not detected";

        assertFalse(NumberUtils.startsWithPrefixInList(givenString, allowedPrefixes).isPresent(), errorMessage);
    }
}
