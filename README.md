# Java Assignment: Natural Numbers Interpretation
## Spring boot application that validates numbers and interprets variations of given numbers.

Exposes 2 REST endpoints:
* http://localhost:8080/validate

Receives list of phone numbers separated by single space character and validates them

Returns each formatted number with a VALID or INVALID message

_Sample Request:_
```bash 
curl \
-d '{"phoneNumbers": [
        "30 2 5 58",
        "2 10 69 30 6 6 4",
        "2 10 69 30 6 60 4",
        "0 0 30 69 74 0 9 22 52",
        "234325234524s",
        "",
        "foo bar"
    ]}' -H 'Content-Type: application/json' http://localhost:8080/validate
```

_Sample Response:_
```json
{
       "phoneNumbersValidations": [
           "21069306604 [phone number: INVALID]",
           "302558 [phone number: INVALID]",
           "foobar [phone number: INVALID]",
           "00306974092252 [phone number: VALID]",
           " [phone number: INVALID]",
           "2106930664 [phone number: VALID]",
           "234325234524s [phone number: INVALID]"
       ]
   }
```
* http://localhost:8080/interpretations

Receives list of phone numbers separated by single space character and validates them. 

Returns all variations of each number with a VALID or INVALID message. 
Variations are based on how numbers are pronounced and possible interpretations of the pronunciation.
_Sample Request:_
```bash 
curl \
-d '{"phoneNumbers": [
        "30 2 5 58",
        "2 10 69 30 6 6 4",
        "2 10 69 30 6 60 4",
        "0 0 30 69 74 0 9 22 52",
        "234325234524s",
        "",
        "foo bar"
    ]}' -H 'Content-Type: application/json' http://localhost:8080/interpretations
```

_Sample Response:_
```json
{
    "interpretedAndValidatedPhoneNumbers": {
        "30 2 5 58": [
            "3025508 [phone number: INVALID]",
            "32558 [phone number: INVALID]",
            "302558 [phone number: INVALID]",
            "325508 [phone number: INVALID]"
        ],
        "": [
            " [phone number: INVALID]"
        ],
        "2 10 69 30 6 6 4": [
            "210693664 [phone number: INVALID]",
            "2106930664 [phone number: VALID]",
            "21060930664 [phone number: INVALID]",
            "2106093664 [phone number: VALID]"
        ],
        "2 10 69 30 6 60 4": [
            "21069306604 [phone number: INVALID]",
            "210609306604 [phone number: INVALID]",
            "210693664 [phone number: INVALID]",
            "2106936604 [phone number: VALID]",
            "2106930664 [phone number: VALID]",
            "21060930664 [phone number: INVALID]",
            "21060936604 [phone number: INVALID]",
            "2106093664 [phone number: VALID]"
        ],
        "foo bar": [
            "foobar [phone number: INVALID]"
        ],
        "0 0 30 69 74 0 9 22 52": [
            "0030697040920252 [phone number: INVALID]",
            "00306970409202502 [phone number: INVALID]",
            "00306097040920252 [phone number: INVALID]",
            "00306974092252 [phone number: VALID]",
            "00306097409202502 [phone number: INVALID]",
            "003069704092252 [phone number: INVALID]",
            "0030697040922502 [phone number: INVALID]",
            "003060974092252 [phone number: INVALID]",
            "0030697409202502 [phone number: INVALID]",
            "0030609704092252 [phone number: INVALID]",
            "003069740922502 [phone number: INVALID]",
            "00306097040922502 [phone number: INVALID]",
            "003060970409202502 [phone number: INVALID]",
            "003069740920252 [phone number: INVALID]",
            "0030609740920252 [phone number: INVALID]",
            "0030609740922502 [phone number: INVALID]"
        ],
        "234325234524s": [
            "234325234524s [phone number: INVALID]"
        ]
    }
}
```